# Semestralni projekt z predmetu BI-VWM: Kolaborativni filtrovani
## Navod na spusteni
Naklonujte si repozitar
```bash
git clone git@gitlab.fit.cvut.cz:tokarart/collaborative-filtering-.git
cd collaborative-filtering-
```
Udelejte virtualni prostredi
```bash
python -m venv venv
```
a aktivujte to
```bash
#windows
venv\Scripts\activate

#linux
source venv/bin/activate
```
Nainstalujte potrebne knihovny
```bash
pip install -r requirements.txt
```
A spustte aplikaci
```bash
python main.py
```

