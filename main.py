from flask import Flask, render_template, flash, redirect, url_for, request
from pymongo import MongoClient
from forms import RegistrationForm, LoginForm
from flask_bcrypt import Bcrypt
from scipy import sparse
from recommender import Recommender
from math import ceil
# App
app = Flask(__name__)
app.config["SECRET_KEY"] = "74498289e3f013e45e4a910ba7cb7a3f"
# Password encrypt module
bcrypt = Bcrypt(app)
# Mongo server
cluster = MongoClient(
    "mongodb+srv://shared_user:collab_filter@cluster0-hh17p.mongodb.net/test?retryWrites=true&w=majority")
# Films
films_db = cluster["films"]
films_collection = films_db["films"]
# Users
users_db = cluster["users"]
users_collection = users_db["users"]
# Ratings
ratings_db = cluster["ratings"]
ratings_collection = ratings_db["ratings"]
# Active user
active_user = {}
# Films to display
films = {}


# Method to get all films rated by active user
def get_rated_films():
    liked_films = []
    ratings = ratings_collection.find({'username': active_user['username']})

    for r in ratings:
        movie_key = r['movie_key']
        film = films_collection.find_one({"movie_key": movie_key})
        film['user_rating'] = r['rating']
        liked_films.append(film)

    return liked_films


# Get 9 random films for main feed
@app.route("/random_movies")
def random_movies():
    rand_films = films_collection.aggregate([{"$sample": {"size": 9}}])
    global films
    films = list(rand_films)
    return redirect(url_for("home"))


# Method to display feed with random films
@app.route("/home", methods=["GET", "POST"])
def home():
    # Redirect to login page if no logged user
    if not active_user:
        return redirect(url_for("login"))

    if request.method == "POST":
        # Search for film in database by name
        if "searchFilm" in request.form:
            text = request.form['searchFilm']
            films_collection.create_index([('title', 'text')])
            global films
            films = list(films_collection.find(
                {
                    "$text": {"$search": text}
                }
            ).sort([("popularity", -1)]).limit(10))
        # Get user rating from form and insert it into ratings database
        elif "rating" in request.form:
            movie_key = int(request.form['movie_key'])
            film = films_collection.find_one({'movie_key': movie_key})
            rating = float(request.form['rating'])
            flash(f"Your rating {rating} on film {film['title']} stored.", "success")
            ratings_collection.insert_one({
                'username': active_user['username'],
                'movie_key': movie_key,
                'rating': rating
            })

    return render_template("home.html",
                           title="Random films",
                           films=films,
                           user=active_user,
                           placeholder="Search in all films...")


# Logout
@app.route("/logout")
def logout():
    global active_user
    active_user = None
    return redirect(url_for("/"))


# Method to display films rated by active user
@app.route("/likes", methods=["GET", "POST"])
def likes():
    # Redirect to login page if no logged user
    if not active_user:
        return redirect(url_for("login"))
    # Get list of rated films
    liked_films = get_rated_films()

    if request.method == "POST":
        # Search in rated films by name
        if "searchFilm" in request.form:
            text = request.form['searchFilm']
            liked_films = [film for film in liked_films if text.lower() in film['title'].lower()]
        # Change user rating to already rated film
        elif "rating" in request.form:
            movie_key = int(request.form['movie_key'])
            rating = float(request.form['rating'])
            # Update info in DB
            ratings_collection.find_one_and_update({'username': active_user['username'], 'movie_key': movie_key},
                                                   {"$set": {'rating': rating}})
            # Update info to display
            for film in liked_films:
                if film['movie_key'] == movie_key:
                    flash(f"Your rating {rating} on film {film['title']} stored.", "success")
                    film['user_rating'] = rating
                    break

    return render_template("likes.html",
                           title="Films you rated",
                           films=liked_films,
                           user=active_user,
                           placeholder="Search in rated films...",
                           amount=len(liked_films),
                           deck_amount=ceil(len(liked_films) / 3))


# Method which represents registration system
@app.route("/register", methods=["GET", "POST"])
def register():
    # Create registration form
    form = RegistrationForm()
    # Validation
    if form.validate_on_submit():
        # Check if username and email are not already used
        if not users_collection.find_one({"username": form.username.data}) \
                and not users_collection.find_one({"email": form.email.data}):
            # Hash password
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode("utf-8")
            # Add user to DB
            users_collection.insert_one({"username": form.username.data,
                                         "email": form.email.data,
                                         "password": hashed_password})
            flash("Your account has been created, you can log in now!", "success")
            return redirect(url_for("login"))
        else:
            flash("Registration failed, this username or email is already used!", "danger")

    return render_template("register.html", title="Register", form=form, user=active_user)


# Method which represents login system
@app.route("/", methods=["GET", "POST"])
@app.route("/login", methods=["GET", "POST"])
def login():
    # Set active user as none
    global active_user
    active_user = None
    # Create login form
    form = LoginForm()
    # Validation
    if form.validate_on_submit():
        user = users_collection.find_one({"email": form.email.data})
        if user and bcrypt.check_password_hash(user['password'], form.password.data):
            flash(f"You have been logged in! Welcome {user['username']}!", "success")
            # Set current user as active
            active_user = user
            return redirect(url_for("random_movies"))
        else:
            flash("Login unsuccessful. Please check email and password!", "danger")

    return render_template("login.html", title="Login", form=form, user=active_user)


# Method to display recommendations
@app.route('/recommend', methods=["GET"])
def recommend():
    # Get list of films rated by active user
    rated_films = get_rated_films()
    ratings = {film['movie_key']: film['user_rating'] for film in rated_films}
    # Evaluate recommendations
    result = rec.recommend(ratings)
    # Find metadata about rated films
    films_rec = list(films_collection.find({'movie_key': {'$in': result}}).sort([('vote_count', -1)]).limit(12))

    return render_template("recommend.html",
                           title="Recommendations",
                           films=films_rec,
                           amount=len(films_rec),
                           deck_amount=ceil(len(films_rec) / 3),
                           user=active_user,
                           placeholder="Search in recommended films...")


if __name__ == "__main__":
    # Load matrix of user ratings
    matrix = sparse.load_npz("./datasets/users_x_movies.npz")
    # Create instance of Recommendation system class
    rec = Recommender(matrix)
    # Run app
    app.run(debug=True)
