import numpy as np
from scipy import sparse
import json
from operator import itemgetter


def read_matrix(file_name: str):
    """
    function loads sparse matrix and provides it to Recommender class
    returns: sparse_matrix object
    """
    return sparse.load_npz(file_name)


class Recommender(object):
    """
    class provides app with recommendation system
    """

    def __init__(self, matrix):
        self.matrix = matrix # sparse matrix object
        self.seen = set()  # seen films by user
        self.indexes_movies = {}  # used to convert index of a column to movie_key
        self.movies_indexes = {}  # used to convert movie_key to index
        self.active_user_vector = None  # vector of user ratings

        # read all movie_keys and their indexes as columns
        with open("./datasets/movie_keys.json", mode="r") as fp:
            self.indexes_movies = dict(json.load(fp))

        #  construct dictionaries
        self.indexes_movies = dict(((int(key), value) for key, value in self.indexes_movies.items()))
        self.movies_indexes = dict(((value, key) for key, value in self.indexes_movies.items()))

    def get_user_vector(self, user_index: int):
        #  returns vector of specific user
        return np.asarray(self.matrix.getrow(user_index).todense())

    def construct_user_vector(self, rated_films: dict):
        #  construct vector of active user ratings
        user_vector = np.zeros_like(self.get_user_vector(1))

        for movie_key, rating in rated_films.items():
            index = int(self.movies_indexes[movie_key])
            user_vector[0, index] = float(rating)
            self.seen.add(int(movie_key))

        self.active_user_vector = user_vector

    def cosine_similarity(self, user_1, user_2):
        #  calculates cosine similarity between two users
        dot_prod = np.dot(user_1, user_2.T)
        norm_1 = np.linalg.norm(user_1)
        norm_2 = np.linalg.norm(user_2)

        return dot_prod / (norm_1 * norm_2)

    def calculate_similarity(self, u1, u2, method="centered_cosine"):
        if method == "centered_cosine":
            # mean rating for each user
            user_1_mean = np.nanmean(u1)
            user_2_mean = np.nanmean(u2)

            u1[np.isnan(u1)] = 0
            u2[np.isnan(u2)] = 0

            # normalized vectors of ratings
            u1 = u1 - user_1_mean
            u2 = u2 - user_2_mean

            return self.cosine_similarity(u1, u2)

        elif method == "cosine":
            u1[np.isnan(u1)] = 0
            u2[np.isnan(u2)] = 0
            return self.cosine_similarity(u1, u2)

        else:
            raise ValueError("Wrong similarity method")

    def neighbors(self, K=5):
        """
        method returns list of K most similar users to active one
        """
        res = {}
        for i in range(self.matrix.shape[0]):
            s = self.calculate_similarity(self.active_user_vector, self.get_user_vector(i), method="centered_cosine")
            res[i] = s
        return set(dict(sorted(res.items(), key=itemgetter(1), reverse=True)[:K]).keys())

    def recommend(self, rated_films, K=10):
        """
        method gets list of most similar users and takes films,
        which were higly rated by them (rating >= 4.5).

        returns list of keys of those films
        """
        result = set()
        self.construct_user_vector(rated_films)
        films_to_recommend = set()
        neighbors = self.neighbors()
        for n in neighbors:
            films_to_recommend.update([j for i in np.where(self.get_user_vector(n) >= 4.5) for j in i])

        for film in films_to_recommend:
            result.add(self.indexes_movies[film])

        result = result.difference(self.seen)
        return list(result)
